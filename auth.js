 const jwt = require('jsonwebtoken');
 const secret = "CourseBookingApi";

 module.exports.createAccessToken = (user) => {

 	console.log(user);

 	const data = {

 		id: user._id,
 		email: user.email,
 		isAdmin: user.isAdmin
 	}

 	return jwt.sign(data,secret,{});
 }

module.exports.verify = (req,res,next) =>{

	// console.log(req.headers.authorization);
	let token = req.headers.authorization;
	// console.log(token);
	// if no token is passed, req.headers.authorization is undefined.
	if(typeof token === "undefined"){
		return res.send({auth: "Failed. No Token"});
	} else {
		// extract token and remove "Bearer "
		token=token.slice(7,token.length);
		console.log(token);

		// verify legitimacy of token
		jwt.verify(token,secret,function(err,decodedToken){

			if(err){
				return res.send({
					auth:"Failed",
					message: err.message
				});
			} else {
				console.log(decodedToken);//contains data payload from our token
				// will add new property in the req object called error.
				req.user = decodedToken;
				next()
			}
		})
	}
}

module.exports.verifyAdmin = (req,res,next) =>{

	if(req.user.isAdmin){
		next();
	} else {
		return res.send({
			auth:"Failed",
			message: "Action Forbidden"
		})
	}
}

module.exports.verifyNonAdmin = (req,res,next) =>{

	if(req.user.isAdmin){
		return res.send({
			auth:"Failed",
			message: "Action Forbidden"
		})
	} else {
		next(); 
	}
}