const express = require('express');
const router = express.Router();
const userControllers = require('../controllers/userControllers');
const {

	registerUser,
	loginUser,
	getSingleUser,
	getAllUsers,
	updateProfile,
	enroll,
	getEnrollments,
	checkEmailExists

} = userControllers;
const auth = require('../auth');
const {verify} = auth;

router.post('/', registerUser);
router.post('/login', loginUser);
router.get('/getUserDetails', verify, getSingleUser);
router.get('/', getAllUsers);
router.put('/updateProfile',verify, updateProfile);
router.put('/enroll',verify,enroll);
router.get('/getEnrollments',verify,getEnrollments);


// check email exist route
router.post('/checkEmailExists',checkEmailExists);

module.exports = router;