const express = require('express');
const router = express.Router();
const courseControllers = require('../controllers/courseControllers');
const {

	addCourse,
	getAllCourses,
	getActiveCourses,
	getSingleCourse,
	updateCourse,
	archiveCourse,
	activateCourse,
	getEnrollees

} = courseControllers;
const auth = require('../auth');
const{verify,verifyAdmin} = auth;

router.post('/',verify,verifyAdmin,addCourse);
router.get('/',verify,verifyAdmin,getAllCourses);
router.get('/getActiveCourses',getActiveCourses);
router.get('/getSingleCourse/:id',getSingleCourse);
router.put('/:id',verify,verifyAdmin,updateCourse);
router.put('/archive/:id',verify,verifyAdmin,archiveCourse);
router.put('/activate/:id',verify,verifyAdmin,activateCourse);
router.get('/getEnrollees/:id',verify,verifyAdmin,getEnrollees);


module.exports = router;