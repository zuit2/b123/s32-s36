const User = require ('../models/User');
const Course = require ('../models/Course');
const bcrypt = require('bcrypt');

const auth = require('../auth');
const {createAccessToken} = auth;

module.exports.registerUser = (req,res) =>{

	console.log(req.body);
	if(req.body.password.length <8) return res.send({message: "Password is too short."})

	const hashedPW = bcrypt.hashSync(req.body.password,10);
	console.log(hashedPW);

	let newUser = new User({

		firstName : req.body.firstName,
		lastName : req.body.lastName,
		mobileNo : req.body.mobileNo,
		email : req.body.email,
		password : hashedPW

	})
	newUser.save()
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

module.exports.loginUser = (req,res) => {


	/*find user by mail
	if user, respond to client
	if no user, send message to client
	if user pw correct, generate key to access app. if not, turn away, send msg to client
	*/
	
	console.log(req.body)

	User.findOne({email: req.body.email})
	.then(result => {

		if(result === null){
			return res.send({message: "No User Found."})
		} else {
			// console.log(req.body.email);
			// console.log(req.body.password);

			const isPasswordCorrect = bcrypt.compareSync(req.body.password,result.password);
			console.log(isPasswordCorrect);

			if(isPasswordCorrect){
				// console.log("We will generate a key.") 
				return res.send({accessToken: createAccessToken(result)});
			} else {
				return res.send({message: "Password is incorrect."})
			}
		}
		
	})
	.catch(err => res.send(err))

}

module.exports.getSingleUser = (req,res) =>{

	console.log(req.user);

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
}

module.exports.getAllUsers = (req,res) =>{

	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
}

module.exports.updateProfile = (req,res) => {

	console.log(req.user);

	let updates = {

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo
	}

	User.findByIdAndUpdate(req.user.id,updates,{new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err))

}

module.exports.enroll = async (req,res) => {

	// console.log(req.user.id)
	// console.log(req.body.courseId)

	if(req.user.isAdmin){
		return res.send({
			auth:"Failed",
			message: "Action Forbidden"
		})
	}

	let isUserUpdated = await User.findById(req.user.id).then(user =>{

		// console.log(user.enrollments);
		// add the courseId in the user's enrollment array:
		user.enrollments.push({courseId: req.body.courseId});

		return user.save()
		.then(user => true)
		.catch(err => err.message)
	})

	console.log(isUserUpdated);

	// end the request/response when isUserUpdated does not return true.
	if(isUserUpdated !== true) return res.send(isUserUpdated);

	let isCourseUpdated = await Course.findById(req.body.courseId).then(course =>{

		// add the logged in user's id into the course's enrollees subdocument array:
		course.enrollees.push({userId: req.user.id})

		return course.save()
		.then(user => true)
		.catch(err => err.message)
	})

	console.log(isCourseUpdated);
	if (isCourseUpdated !== true) return res.send(isCourseUpdated);

	if(isUserUpdated && isCourseUpdated) return res.send("Enrolled Successfully.");

}	

module.exports.getEnrollments = (req,res) =>{
	console.log(req.user);
	User.findById(req.user.id)
	.then(foundUser => res.send(foundUser.enrollments))
	.catch(err => res.send(err));
}

module.exports.checkEmailExists = (req,res) => {

	User.findOne({email: req.body.email})
	.then(result => {

		if(result === null){
			return res.send({
				isAvailable: true,
				message: "Email Available."
			})
		} else {
			return res.send({
				isAvailable: false,
				message: "Email is already registered."})
		}
	})
	.catch(err => res.send(err));
}