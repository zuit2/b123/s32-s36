const Course = require('../models/Course');

module.exports.addCourse = (req,res) =>{

	console.log(req.body);
	let newCourse = new Course({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	})

	newCourse.save()
	.then(result => res.send(result))
	.catch(err => res.send(err))
};

module.exports.getAllCourses = (req,res) =>{

	Course.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

module.exports.getActiveCourses = (req,res) =>{

	Course.find({isActive:true})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

module.exports.getSingleCourse = (req,res) =>{
	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

module.exports.updateCourse = (req,res) =>{

	console.log(req.body);

	let updates = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Course.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err))

}

module.exports.archiveCourse = (req,res) =>{

	let updates = {
		isActive: false
	}
	Course.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(archivedCourse => res.send(archivedCourse))
	.catch(err => res.send(err))
}

module.exports.activateCourse = (req,res) =>{

	let updates = {
		isActive: true
	}
	Course.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(activatedCourse => res.send(activatedCourse))
	.catch(err => res.send(err))
}

module.exports.getEnrollees = (req,res) =>{

	console.log(req.params.id);
	Course.findById(req.params.id)
	.then(foundCourse => res.send(foundCourse.enrollees))
	.catch(err => res.send(err));
}