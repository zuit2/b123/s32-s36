const express = require("express");
const mongoose = require("mongoose");

const cors = require('cors');

const app = express();
const port = process.env.PORT || 4000;

mongoose.connect("mongodb+srv://efrenjrdev:batch123@sandboxdb.2ttig.gcp.mongodb.net/bookingAPI?retryWrites=true&w=majority"
	,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;
db.on("error",console.error.bind(console, "Connection Error"));
db.once("open", ()=> console.log("Connected to MongoDB"));

app.use(express.json());

const courseRoutes = require('./routes/courseRoutes');
app.use('/courses',courseRoutes)

const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes)




app.listen(port, ()=>console.log(`Server is running at port ${port}`));